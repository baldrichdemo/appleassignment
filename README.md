# Take Home Coding Assignment

The purpose of this assignment was to create a production ready web service which combines two existing web services.
1. Fetch a random name from http://uinames.com/api/
2. Fetch a random Chuck Norris joke from http://api.icndb.com/jokes/random?
firstName=John&lastName=Doe&limitTo=[nerdy]
3. Combine the results and return them to the user.

## Recent Improvements
* Increased throughput by caching Jokes and Names in the background. 
* Implemented a rate limiting option
* Increased TPS from 480 to 4850

## Build and Run From Source
```
git clone https://gitlab.com/baldrichdemo/appleassignment
mkdir -p $GOPATH/src/gitlab.com/baldrichdemo/
mv appleassignment/ $GOPATH/src/gitlab.com/baldrichdemo/
cd $GOPATH/src/gitlab.com/baldrichdemo/appleassignment
go build
```

```
./appleassignment
```

## Use Docker
The docker image sets the logging to /dev/stdout. The image configuration can be seen in the Configuration Subsection.

```
docker pull baldrichdemo/appleassignment:3.0
docker run -p 5000:5000 baldrichdemo/appleassignment:3.0
```

## Configuration
```
vi conf.yaml

Server:
  ListenPort: ":5000" # Include colon (:) at the begining of port numberlimmiter
  Limiter: 
    Enabled: false # Enable or disable a rate limiter
    TPS: 10 # limit 10 tps per client
    Burst: 25 # limit a 25 burst
Logging:
  File: "/dev/stdout" # Where do you want the server to log to? 
  Debug: true # If in debug mode the logger will print 200 OK requests, and general debugging statements. If not in debug mode, the logger will only print errors. 
```

## Benchmark

Note: The "length failure" indicates the length of the response was different, which is expected because our results are dynamic.

```
ab -c 500 -n 500 http://localhost:5000/
This is ApacheBench, Version 2.3 <$Revision: 1757674 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)
Completed 100 requests
Completed 200 requests
Completed 300 requests
Completed 400 requests
Completed 500 requests
Finished 500 requests


Server Software:        
Server Hostname:        localhost
Server Port:            5000

Document Path:          /
Document Length:        42 bytes

Concurrency Level:      500
Time taken for tests:   0.103 seconds
Complete requests:      500
Failed requests:        495
   (Connect: 0, Receive: 0, Length: 495, Exceptions: 0)
Total transferred:      100489 bytes
HTML transferred:       38902 bytes
Requests per second:    4862.58 [#/sec] (mean)
Time per request:       102.826 [ms] (mean)
Time per request:       0.206 [ms] (mean, across all concurrent requests)
Transfer rate:          954.37 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        6   12   3.7     11      20
Processing:    11   49  23.5     46      80
Waiting:        5   49  23.8     46      80
Total:         26   61  20.6     59      99

Percentage of the requests served within a certain time (ms)
  50%     59
  66%     79
  75%     82
  80%     83
  90%     84
  95%     85
  98%     89
  99%     98
 100%     99 (longest request)
```


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
