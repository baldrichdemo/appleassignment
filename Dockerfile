FROM golang:1.11
 
RUN mkdir -p /app
 
WORKDIR /app
 
ADD appleassignment /app
ADD conf.yaml /app
  
CMD ["./appleassignment"]