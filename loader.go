package main

import (
	"encoding/json"
	"strings"
	"time"

	resty "gopkg.in/resty.v1"
)

var (
	namesCache = make(chan string, 7500) // A data store to retrieve names
	jokesCache = make(chan string, 7500) // A data store to retrieve jokes
	comboCache = make(chan string, 7500) //A data store to combine them
)

//A Helper type to organize functions related to the cache loader.
type Loader struct{}

// Struct representing the JSON value received for a random name.
type RandName struct {
	Name    string `json:"name"`
	Surname string `json:"surname"`
	Gender  string `json:"gender"`
	Region  string `json:"region"`
}

// A helper type, array of names
type RandNames []RandName

// Struct representing the JSON value of a random joke.
type RandChuckNorrisJoke struct {
	Type  string       `json:"type"`
	Value []JokeValues `json:"value"`
}

// Struct representing the Joke's value, for the structure above.
type JokeValues struct {
	Id         int      `json:"id"`
	Joke       string   `json:"joke"`
	Categories []string `json:"categories"`
}

//A function that starts caching for names, jokes, and their combo.
func (l *Loader) StartCaching() {
	l.CacheNames()
	l.CacheJokes()
	l.CacheCombo()
}

//A function that spins off a routine caching names
func (l *Loader) CacheNames() {
	go func() {
		for true {

			rNames := RandNames{}
			resp, err := resty.R().Get("https://uinames.com/api/?amount=500")
			if err != nil {
				logger.Error(err)
			}

			rNames, err = rNames.unMarshalJSON(resp.Body())
			if err != nil {
				logger.Error(err)
				if debug {
					logger.Debug("Unexpected Payload: " + string(resp.Body()))
				}
			} else {
				go func(names RandNames) {
					for _, randName := range names {
						name := randName.Name + " " + randName.Surname
						namesCache <- name
					}
				}(rNames)
			}
		}
	}()
}

//A function that spins off a routine caching jokes.
func (l *Loader) CacheJokes() {
	go func() {
		for true {
			rJokes := RandChuckNorrisJoke{}
			resp, err := resty.R().Get("http://api.icndb.com/jokes/random/100?firstName=John&lastName=Doe&limitTo=[nerdy]")
			if err != nil {
				logger.Error(err)
			}

			rJokes, err = rJokes.unMarshalJSON(resp.Body())
			if err != nil {
				logger.Error(err)
				if debug {
					logger.Debug("Unexpected Payload: " + string(resp.Body()))
				}
			} else {
				go func(jokes RandChuckNorrisJoke) {
					for _, randJoke := range jokes.Value {
						jokesCache <- randJoke.Joke
					}
				}(rJokes)
			}
		}
	}()
}

//A function to select over a name then a joke and combine them to send on the combo channel.
func (l *Loader) CacheCombo() {
	go func() {
		for {
			select {
			case name := <-namesCache:
				joke := <-jokesCache
				joke = strings.Replace(joke, "John Doe", name, -1)
				comboCache <- joke
			case <-time.After(1 * time.Second):
				logger.Error("Not finding a name and joke to combine.")
			}
		}
	}()
}

//A helper function to unmarshal bytes into the RandName struct
func (rn *RandNames) unMarshalJSON(v []byte) ([]RandName, error) {
	rNames := []RandName{}
	err := json.Unmarshal(v, &rNames)
	if err != nil {
		return rNames, err
	}

	return rNames, nil
}

//A helper function to unmarshal bytes into the RandChuckNorrisJoke struct
func (rn *RandChuckNorrisJoke) unMarshalJSON(v []byte) (RandChuckNorrisJoke, error) {
	rJokes := RandChuckNorrisJoke{}
	err := json.Unmarshal(v, &rJokes)
	if err != nil {
		return rJokes, err
	}

	return rJokes, nil
}
