package main

/*
Author: Brice Aldrich
Description: Take Home Coding Assignment
Version: 3.0
*/

import (
	"encoding/json"
	"fmt"
	"golang.org/x/time/rate"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	"github.com/op/go-logging" // Set up a logger, using github.com/op/go-logging
	"github.com/spf13/viper"   // Viper is a neat library I use ocasionally for lazy reading configs.
)

// Structure to decribe an HTTP Server, mainly if your program handles multiple servers, in this case it's only one.
type HttpServer struct {
	Port    string
	Limiter Limit
	Router  *mux.Router
}

type Limit struct {
	Enabled bool
	Limit   *rate.Limiter
}

var (
	limiter = Limit{Enabled: false}
	logger  = logging.MustGetLogger("example")
	debug   = false
)

func main() {
	viper.SetConfigName("conf")
	viper.AddConfigPath("./")
	viper.SetConfigType("yaml")
	err := viper.ReadInConfig()
	if err != nil {
		fmt.Printf("Fatal error config file: %s \n", err)
	}

	err = setUpLogging()
	if err != nil {
		fmt.Printf("Could not setup logging: %s \n", err)
	}

	debug = viper.GetBool("Logging.Debug")
	port := viper.GetString("Server.ListenPort")
	if port == "" {
		port = ":5000"
	}

	limiterEnabled := viper.GetBool("Server.Limiter.Enabled")
	if limiterEnabled {
		limiter.Enabled = true
		tps := viper.GetInt("Server.Limiter.TPS")
		burst := viper.GetInt("Server.Limiter.Burst")
		if tps != 0 && burst != 0 {
			limit := rate.Limit(tps)
			limiter.Limit = rate.NewLimiter(limit, burst)
		} else {
			limiter.Limit = rate.NewLimiter(10, 25)
		}
	}

	loader := Loader{}
	loader.StartCaching()
	if debug {
		logger.Debug(fmt.Sprintf("Running loader to cache results from jokes and names"))
	}

	router := mux.NewRouter()
	serv := HttpServer{Port: port, Limiter: limiter, Router: router}
	router.HandleFunc("/", serv.Handle).Methods("GET")

	serv.Start()
}

// A Function to start an Http Server with the values of the HttpServer struct that calls it.
func (hs *HttpServer) Start() {
	logger.Info("Starting server on http://localhost:" + hs.Port)

	if hs.Limiter.Enabled {
		logger.Info("Rate limiting client calls")
		err := http.ListenAndServe(hs.Port, hs.limit(hs.Router))
		if err != nil {
			logger.Error("Internal Server Error: " + err.Error())
		}
	} else {
		err := http.ListenAndServe(hs.Port, hs.Router)
		if err != nil {
			logger.Error("Internal Server Error: " + err.Error())
		}
	}
}

// A function to handle the client limit for an http server.
func (hs *HttpServer) limit(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if hs.Limiter.Limit.Allow() == false {
			http.Error(w, http.StatusText(429), http.StatusTooManyRequests)
			return
		}

		next.ServeHTTP(w, r)
	})
}

// The http callback function used for /.
func (hs *HttpServer) Handle(w http.ResponseWriter, r *http.Request) {
	select {
	case res := <-comboCache:
		hs.respondWithJson(w, http.StatusOK, res)
		if debug {
			logger.Debug("Client request from " + r.RemoteAddr + " success: " + res)
		}
	case <-time.After(1 * time.Second):
		hs.respondWithError(w, http.StatusInternalServerError, "Uh oh! I'm all out of jokes! Please try back again soon!")
		logger.Error("Client request from " + r.RemoteAddr + " failed: no jokes to tell!")
	}
}

//A function to handle an error response 500
func (hs *HttpServer) respondWithError(w http.ResponseWriter, code int, msg string) {
	hs.respondWithJson(w, code, map[string]string{"error": msg})
}

//A function to handle the json response back
func (hs *HttpServer) respondWithJson(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(code)
	w.Write(response)
}

//A helper function to setup up logging, really just to make main less cluttered.
func setUpLogging() error {
	fileName := viper.GetString("Logging.File")
	if fileName == "" {
		fileName = "./appleassignment.log"
	}

	logFile, err := os.OpenFile(fileName, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		return err
	}

	var format = logging.MustStringFormatter(
		`%{color}%{time:15:04:05.000} %{shortfunc} ▶ %{level:.4s} %{id:03x}%{color:reset} %{message}`,
	)

	loggertmp := logging.NewLogBackend(logFile, "", 0)
	backendFormatter := logging.NewBackendFormatter(loggertmp, format)
	logging.SetBackend(backendFormatter)

	return nil
}
